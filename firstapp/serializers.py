from rest_framework import serializers
from .models import Category, Post
from django.contrib.auth.models import User


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        # likhna jrori becuz error a rhi k eh version deprecateho gya eh line likho ta likya
        fields = '__all__'

        # eda v kr sakde jive modl form ch krde hi apa
        # fields = ['id', 'title','created_at']


class PostSerializer(serializers.ModelSerializer):
    # NOTE : CASE 1--->

    # owner = serializers.StringRelatedField(source='owner.username')

    # NOTE : CASE 2---->

    # NOTE : niche vali line vang kna jada btter h te concise v

    owner = serializers.StringRelatedField(many=False)

    # NOTE : CASE 3 ---->

    # category field ch id show hon de jga categry da name shoW hona
    # category = serializers.StringRelatedField(many=False)

    # NOTE : CASE 4 ---> id show honi hun ,category da name show ni hona

    # category = serializers.PrimaryKeyRelatedField(many=False, read_only=True)

    # NOTE : CASE 5: VERY IMP : HYPERLINKED FIELD

    # urls.py toh dekhya name = single category vala
    # category = serializers.HyperlinkedRelatedField(
    # many=False, read_only=True, view_name='single_category')

    # NOTE  CASE 5 (II) SECOND APPROCH

    # category = serializers.HyperlinkedRelatedField(
    # many=False, queryset=Category.objects.all(), view_name='single_category')

    # NOTE : CASE 6 ---->

    category = serializers.SlugRelatedField(
        many=False, read_only=True, slug_field='title')

    class Meta:
        model = Post
        # fields = '__all__'

        fields = ['id', 'title', 'body', 'owner', 'category']


class UserSerializer(serializers.ModelSerializer):
    posts = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Post.objects.all())

    class Meta:
        model = User
        fields = ['id', 'username', 'posts']
